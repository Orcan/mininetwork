using System;
using UnityEngine;using UnityEngine.UI;
using Object = UnityEngine.Object;
using System.Collections;
using System.Collections.Generic; 
using UnityEngine.Events; 
 namespace UnityStandardAssets.Utility
 
{

    public class ActivateTrigger : MonoBehaviour
    {
        // A multi-purpose script which causes an action to occur when
        // a trigger collider is entered.

            public UnityEvent InvokeMetodObject = new UnityEvent();
        // A multi-purpose script which causes an action to occur when
        // a trigger collider is entered.
        
        [Flags]
        public enum UseMethod
        {
            OnTriggerEnter = 1 << 0
                ,
            Start = 1 << 1,


            OnTriggerExit = 2 << 2,
//добавить ExitTriger
              
            OnTriggerStay = 3 << 3,
        }
          public UseMethod UseMethods = UseMethod.OnTriggerEnter;
        public enum Mode
        {
            Trigger = 0,    // Just broadcast the action on to the target
            Replace = 1,    // replace target with source
            Activate = 2,   // Activate the target GameObject
            Enable = 3,     // Enable a component
            Animate = 4,    // Start animation on target
            Deactivate = 5  // Decativate target GameObject
            ,MngSceneChengeCoint=6
            ,MngSceneMethodChengeCoint = 7
            ,Invoke = 8
            ,MngSceneMethodChengeString = 9
            ,InvokeMetod = 10
        }
  
 
        public float VarFloat1 = 0;
         public float VarFloat2 = 0;
         public bool VarBool1 = false;
         public string VarString1 = "false";
        public Mode action = Mode.Activate;         // The action to accomplish
        public GameObject target;                       // The game object to affect. If none, the trigger work on this game object
        public GameObject source;
        public int triggerCount = 1;
        public bool repeatTrigger = false;

        public string tag;

        public void Start(){
         
        }
        private void DoActivateTrigger()
        {
            triggerCount--;

            if (triggerCount == 0 || repeatTrigger)
            {
                Object currentTarget = target ?? gameObject;
                Behaviour targetBehaviour = currentTarget as Behaviour;
                GameObject targetGameObject = currentTarget as GameObject;
                if (targetBehaviour != null)
                {
                    targetGameObject = targetBehaviour.gameObject;
                }

                switch (action)
                {
                    case Mode.Trigger:
                        if (targetGameObject != null)
                        {
                            targetGameObject.BroadcastMessage(VarString1);
                        }
                        break;
                    case Mode.Replace:
                        if (source != null)
                        {
                            if (targetGameObject != null)
                            {
                                Instantiate(source, targetGameObject.transform.position,
                                            targetGameObject.transform.rotation);
                                DestroyObject(targetGameObject);
                            }
                        }
                        break;
                    case Mode.Activate:
                        if (targetGameObject != null)
                        {
                            targetGameObject.SetActive(true);
                        }
                        break;
                    case Mode.Enable:
                        if (targetBehaviour != null)
                        {
                            targetBehaviour.enabled = true;
                        }
                        break;
                    case Mode.Animate:
                        if (targetGameObject != null)
                        {
                            targetGameObject.GetComponent<Animation>().Play();
                        }
                        break;
                    case Mode.Deactivate:
                        if (targetGameObject != null)
                        {
                            targetGameObject.SetActive(false);
                         
                        }
                        break;
                       case Mode.MngSceneChengeCoint:
                       target.GetComponent<ManagerScene>()._coins += VarFloat1;
                        break;
                        
                          case Mode.MngSceneMethodChengeCoint:
                       target.GetComponent<ManagerScene>().MetodChengeCoint(VarFloat1);
                        break;
                             case Mode.MngSceneMethodChengeString:
                       target.GetComponent<ManagerScene>().MetodChengeString(VarString1);
                        break;
                                case Mode.InvokeMetod:
                        if (targetGameObject != null)
                        {
                                InvokeMetodObject.Invoke();
                        }
                          break;
                }
            }
        }


        private void OnTriggerEnter(Collider other)
        {         if ( UseMethods == UseMethod.OnTriggerEnter)
  {        if(tag != "" &&  tag ==other.tag)
            {DoActivateTrigger(); return;} 
  } 
       
 
        }

   private  void OnTriggerExit(Collider other)
        {
            if ( UseMethods == UseMethod.OnTriggerExit)
  {        if(tag != "" &&  tag ==other.tag)
            {DoActivateTrigger(); return;} 
  } 
        }




    }
 
}