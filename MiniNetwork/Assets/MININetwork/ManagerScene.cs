﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;


public class ManagerScene : NetworkBehaviour {
#region Michael code

	public enum CubeState
	{
		none,
		yellow,
		green,
		red
	}
	[SyncVar] 
	public CubeState cubeState = CubeState.none;
	
	
#endregion
	
public GameObject Player1;    // for name 
public string playerName = "Player";
  public Text textNameField; 
public Text _playerNameTXT ;
public float _coins = 0 ;    // для передачи разных переменных через сетку
public Text txt_coins;

public string _string = "" ;
public Text txt_string;

public bool _boolActive = false;
public Text txt_bool;

public Material[] _material;    // for material
public bool[] materialBool;
public GameObject CubeGradient;

public GameObject prefab; // for instance and rotate
public Transform positionInstance;
public GameObject InstanceGO;
public int Povorot =0;

/////////////////////////////////////    Здесь должны передать Имя игрока   /////////////////////////////////////////

	
        
	
		
 		public void Send () { 
		playerName = textNameField.text;  
        _playerNameTXT.text = "NamePlayer: " + playerName;
      //Player1.GetComponent<PlayerName>().Send(); 
		} 


/////////////////////////////////////    Здесь передаем float   /////////////////////////////////////////
public void MetodChengeCoint (float cointChenge){
	_coins += cointChenge;
    txt_coins.text = "Coins: " + _coins;
}




/////////////////////////////////////    Здесь передаем string   /////////////////////////////////////////
public void MetodChengeString (string stringArg){
	_string = stringArg;
	  txt_string.text = "Sring: " + _string;
}

/////////////////////////////////////    Здесь передаем string   /////////////////////////////////////////
public void MetodChengeBool (bool boolArg){
	_boolActive = boolArg;
	  txt_bool.text = "Bool: " + _boolActive;
}




///////////////////////////////////// Здесь Работы с изменениями Материалов изменение цветов по сети    /////////////////////////////////////////
public void EventBlockTrue(int block){ // задумка такая когда заходит одинна платформу  Куб меняет цвет на определенный (в зависимости от платформы) когда заходят оба на 2 платформы одновременно тоЦвет платформы меняется на красный
 materialBool[block] = true;

 //print("materialBool[block]   "+ materialBool[block]);
RunMaterial();
}
public void EventBlockFalse(int block){
 materialBool[block] = false;
RunMaterial();
}

  void RunMaterial(){
	  if (materialBool[0] && materialBool[1])
	  {
		  cubeState = CubeState.red;
	  }
	else{
		if (materialBool[0])
		{
			cubeState = CubeState.green;
		}
		if (materialBool[1])
		{
			cubeState = CubeState.yellow;
		}
		  
		  
	  }
  
}

///////////////////////////////////// Здесь Работы  с Появлениями Юнитов и передача их Движения по сети и Удаление через минуту   /////////////////////////////////////////
public void InstanceAndRotation(){  //тоесть функция должна сказать появись в этой позиции и начинай вращаться в одном из 4 направления

 InstanceGO =   Instantiate(prefab, positionInstance.position, positionInstance.rotation);
Povorot = Random.Range(1,3);
Destroy(InstanceGO, 60);

}





	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
	 
if(InstanceGO != null){  
if(Povorot == 1){   InstanceGO.transform.Rotate(new Vector3(1,0,1)); }
if(Povorot == 2){   InstanceGO.transform.Rotate(new Vector3(0,1,0)); }
if(Povorot == 3){   InstanceGO.transform.Rotate(new Vector3(0,0,1)); }
 if(Povorot == 0){   InstanceGO.transform.Rotate(new Vector3(0,0,0)); }


         }

		 Debug.LogError(cubeState);
		switch (cubeState)
		{
			case CubeState.green:
				CubeGradient.GetComponent<Renderer>().material = _material[0];
				break;
			case CubeState.yellow:
				CubeGradient.GetComponent<Renderer>().material = _material[1];
				break;
			case CubeState.red:
				CubeGradient.GetComponent<Renderer>().material = _material[2];
				break;
		}
	
		

	}
}
